# text-to-image
本项目是基于Google机器学习框架[TensorFlow][1]来开发的项目，实现了基本的场景转换的功能。本项目基于论文[Generative Adversarial Text-to-Image Synthesis][2]实现
的，基于[DC-GAN][3]的理念,实现了原论文中GAN-CLS算法。

![Model architecture](http://i.imgur.com/dNl2HkZ.jpg)

Image Source : [Generative Adversarial Text-to-Image Synthesis][2] Paper

## 环境要求
- Python 2.7.14
- TensorFlow 0.11.0
- h5py
- Theano(请安装dev版本，不要安装0.8.2版本，否则编译器报错)
- scikit-learn
- NLTK

## 数据集
- 请在下载数据集前，在Data文件夹下创建四个空文件夹，分别命名为*flowers*,*samples*,*val_samples*以及*models*,并在生成图像采样和保存模型时
  保持后面三个文件夹为空。
- 之后请运行*download_datasets.py*文件，该文件可以自动下载所需数据。大多数数据可以保证正常下载，但是文本描述数据文件该代码无法下载，请点击该[链接][4]下载
  数据并解压，
  将其中的*text_c10*文件夹复制粘贴到*Data/flowers*目录下。
- 若想要提前运行代码却不想因为训练模型而耗费时间，可以点击该[链接][5]下载预训练模型，并将下载好的文件放入*Data/Models*目录下再运行图像合成部分的脚本。

## 使用方法
- _数据处理:_获取skipthoughts向量：

  `python data_loader.py --data_set="flowers"`
  
- _训练数据_
      * 命令行：`python train.py --data_set="flowers"`
	  
- _生成图像_
	  * 在文本文件中写入文本描述，后将其保存为*Data/sample_captions.txt*，之后生成思维向量：
	  
	  `python generate_thought_vectors.py --caption_file="Data/sample_captions.txt"`
	  
	  * 使用思维向量生成图像：
	  
	  `python generate_images.py --model_path=<复制你的模型所在路径> --n_images=8`

	  
	  * 生成的图像会保存在*Data/val_samples*文件夹之中
  
## 简单图像生成

| Caption        | Generated Images  |
| ------------- | -----:|
| the flower shown has yellow anther red pistil and bright red petals        | ![](http://i.imgur.com/SknZ3Sg.jpg)   |
| this flower has petals that are yellow, white and purple and has dark lines        | ![](http://i.imgur.com/8zsv9Nc.jpg)   |
| the petals on this flower are white with a yellow center        | ![](http://i.imgur.com/vvzv1cE.jpg)   |
| this flower has a lot of small round pink petals.        | ![](http://i.imgur.com/w0zK1DC.jpg)   |
| this flower is orange in color, and has petals that are ruffled and rounded.        | ![](http://i.imgur.com/VfBbRP1.jpg)   |
| the flower has yellow petals and the center of it is brown        | ![](http://i.imgur.com/IAuOGZY.jpg)   |

## 引用
- [Generative Adversarial Text-to-Image Synthesis][2] Paper
- [Generative Adversarial Text-to-Image Synthesis][6] Code
- [DCGAN in Tensorflow][7]

[1]:https://www.tensorflow.org/
[2]:http://arxiv.org/abs/1605.05396
[3]:https://arxiv.org/abs/1511.06434
[4]:https://drive.google.com/file/d/0B0ywwgffWnLLcms2WWJQRFNSWXM/view
[5]:https://bitbucket.org/paarth_neekhara/texttomimagemodel/raw/74a4bbaeee26fe31e148a54c4f495694680e2c31/latest_model_flowers_temp.ckpt
[6]:https://github.com/reedscot/icml2016
[7]:https://github.com/carpedm20/DCGAN-tensorflow
